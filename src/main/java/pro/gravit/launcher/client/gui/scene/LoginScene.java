package pro.gravit.launcher.client.gui.scene;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import pro.gravit.launcher.LauncherEngine;
import pro.gravit.launcher.client.gui.JavaFXApplication;
import pro.gravit.launcher.client.gui.helper.LookupHelper;
import pro.gravit.launcher.client.gui.raw.AbstractScene;
import pro.gravit.launcher.events.request.GetAvailabilityAuthRequestEvent;
import pro.gravit.launcher.request.auth.AuthRequest;
import pro.gravit.launcher.request.auth.GetAvailabilityAuthRequest;
import pro.gravit.launcher.request.update.LauncherRequest;
import pro.gravit.launcher.request.update.ProfilesRequest;
import pro.gravit.utils.helper.LogHelper;
import pro.gravit.utils.helper.SecurityHelper;

import java.io.IOException;
import java.util.List;

public class LoginScene extends AbstractScene {
    public List<GetAvailabilityAuthRequestEvent.AuthAvailability> auth;
    public Node layout;
    public boolean isStartedLogin;

    private class AuthConverter extends StringConverter<GetAvailabilityAuthRequestEvent.AuthAvailability> {

        @Override
        public String toString(GetAvailabilityAuthRequestEvent.AuthAvailability authAvailability) {
            return authAvailability.displayName;
        }

        @Override
        public GetAvailabilityAuthRequestEvent.AuthAvailability fromString(String s) {
            for (GetAvailabilityAuthRequestEvent.AuthAvailability a : auth)
                if (a.displayName.equals(s)) return a;
            return null;
        }
    }

    public LoginScene(JavaFXApplication application) {
        super("scenes/login/login.fxml", application);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void doInit() {
        layout = LookupHelper.lookup(scene.getRoot(), "#layout", "#authPane");
        sceneBaseInit(layout);
        TextField loginField = (TextField) layout.lookup("#login");
        if (application.runtimeSettings.login != null) {
            loginField.setText(application.runtimeSettings.login);
        }
        TextField passwordField = (TextField) layout.lookup("#password");
        if (application.runtimeSettings.encryptedPassword != null) {
            passwordField.getStyleClass().add("hasSaved");
            passwordField.setPromptText(application.getLangResource("runtime.scenes.login.login.password.saved"));
            ((CheckBox) layout.lookup("#savePassword")).setSelected(true);
        }
        if(application.guiModuleConfig.createAccountURL != null)
        {
            ((Hyperlink)layout.lookup("#createAccount")).setOnAction((e) -> {
                application.openURL(application.guiModuleConfig.createAccountURL);
            });
        }
        if(application.guiModuleConfig.forgotPassURL != null)
        {
            ((Hyperlink)layout.lookup("#forgotPass")).setOnAction((e) -> {
                application.openURL(application.guiModuleConfig.forgotPassURL);
            });
        }
        ComboBox<GetAvailabilityAuthRequestEvent.AuthAvailability> authList = (ComboBox<GetAvailabilityAuthRequestEvent.AuthAvailability>) layout.lookup("#combologin");
        authList.setConverter(new AuthConverter());
        ((ButtonBase) layout.lookup("#goAuth")).setOnAction((e) -> contextHelper.runCallback(this::loginWithGui).run());
        //Verify Launcher
        {
            LauncherRequest launcherRequest = new LauncherRequest();
            processRequest(application.getLangResource("runtime.overlay.processing.text.launcher"), launcherRequest, (result) -> {
                if (result.needUpdate) {
                    try {
                        application.securityService.update(result);
                    } catch (IOException e) {
                        LogHelper.error(e);
                    } catch (Throwable ignored) {

                    }
                    try {
                        LauncherEngine.exitLauncher(0);
                    } catch (Throwable e)
                    {
                        Platform.exit();
                    }
                }
                LogHelper.dev("Launcher update processed");
                GetAvailabilityAuthRequest getAvailabilityAuthRequest = new GetAvailabilityAuthRequest();
                processRequest(application.getLangResource("runtime.overlay.processing.text.authAvailability"), getAvailabilityAuthRequest, (auth) -> contextHelper.runInFxThread(() -> {
                    this.auth = auth.list;
                    int authIndex = 0;
                    int i = 0;
                    for (GetAvailabilityAuthRequestEvent.AuthAvailability a : auth.list) {
                        if (a.equals(application.runtimeSettings.lastAuth)) authIndex = i;
                        authList.getItems().add(a);
                        i++;
                    }
                    authList.getSelectionModel().select(authIndex);
                    hideOverlay(0, null);
                }), null);
            }, (e) -> {
                LauncherEngine.exitLauncher(0);
            });
        }
    }

    @SuppressWarnings("unchecked")
    public void loginWithGui() {
        String login = ((TextField) layout.lookup("#login")).getText();
        TextField passwordField = ((TextField) layout.lookup("#password"));
        byte[] encryptedPassword;
        if (passwordField.getText().isEmpty() && passwordField.getPromptText().equals(application.getLangResource("runtime.scenes.login.login.password.saved"))) {
            encryptedPassword = application.runtimeSettings.encryptedPassword;
        } else {
            String password = passwordField.getText();
            try {
                encryptedPassword = encryptPassword(password);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        ComboBox<GetAvailabilityAuthRequestEvent.AuthAvailability> authList = (ComboBox) layout.lookup("#combologin");
        String auth_id = authList.getSelectionModel().getSelectedItem().name;
        boolean savePassword = ((CheckBox) layout.lookup("#savePassword")).isSelected();
        login(login, encryptedPassword, auth_id, savePassword);
    }

    public byte[] encryptPassword(String password) throws Exception {
        return SecurityHelper.encrypt(launcherConfig.passwordEncryptKey, password);
    }

    public void login(String login, byte[] password, String auth_id, boolean savePassword) {
        isStartedLogin = true;
        LogHelper.dev("Auth with %s password ***** auth_id %s", login, auth_id);
        AuthRequest authRequest = new AuthRequest(login, password, auth_id);
        processRequest(application.getLangResource("runtime.overlay.processing.text.auth"), authRequest, (result) -> {
            application.runtimeStateMachine.setAuthResult(result);
            if (savePassword) {
                application.runtimeSettings.login = login;
                application.runtimeSettings.encryptedPassword = password;
            }
            onGetProfiles();

        }, null);
    }

    public void onGetProfiles() {
        processRequest(application.getLangResource("runtime.overlay.processing.text.profiles"), new ProfilesRequest(), (profiles) -> {
            application.runtimeStateMachine.setProfilesResult(profiles);
            contextHelper.runInFxThread(() -> {
                hideOverlay(0, null);
                application.securityService.startRequest();
                if(application.gui.optionsScene != null)
                {
                    try {
                        application.gui.optionsScene.loadAll();
                    } catch (Throwable ex)
                    {
                        LogHelper.error(ex);
                    }
                }
                if(application.getCurrentScene() instanceof LoginScene)
                {
                    ((LoginScene) application.getCurrentScene()).isStartedLogin = false;
                }
                application.setMainScene(application.gui.serverMenuScene);
            });
        }, null);
    }

    public void clearPassword()
    {
        application.runtimeSettings.encryptedPassword = null;
        application.runtimeSettings.login = null;
        TextField loginField = (TextField) layout.lookup("#login");
        TextField passwordField = (TextField) layout.lookup("#password");
        passwordField.getStyleClass().removeAll("hasSaved");
        passwordField.setText("");
        loginField.setText("");
    }
}
