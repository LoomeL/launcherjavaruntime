package pro.gravit.launcher.client.gui.interfaces;

public interface FXMLConsumer {
    String getFxmlPath();
}
